#pragma once
#include <SFML/Graphics.hpp>
#include "src/Character/AnimatedSprite.hpp"
#include <tmx/MapLoader.hpp>

class Character
{
public:
    Character();
    void DefineAnimations();
    void Move(tmx::MapLoader &map);
    bool SetTexture();
    bool DetectCollision(tmx::MapLoader &map, sf::Rect<float> position);
    AnimatedSprite animatedSprite;
    ~Character();
private:
    
    
    Animation* currentAnimation;
    sf::Texture texture;
    sf::Clock frameClock;
    float speed = 80.f;
    bool noKeyWasPressed = true;
    sf::Time frameTime;
    Animation walkingAnimationDown;
    Animation walkingAnimationLeft;
    Animation walkingAnimationRight;
    Animation walkingAnimationUp;
};

