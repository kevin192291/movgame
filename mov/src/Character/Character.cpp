#include "Character.h"

Character::Character()
{
    AnimatedSprite animatedSprite(sf::seconds(0.2), true, false);
    this->animatedSprite = animatedSprite;
    sf::Texture texture;
    if (!texture.loadFromFile("guy.png"))
    {
        //std::cout << "Failed to load player spritesheet!" << std::endl;
    }
    animatedSprite.setPosition(300,300);
}

bool Character::SetTexture()
{
    if (!texture.loadFromFile("guy.png"))
    {
        //std::cout << "Failed to load player spritesheet!" << std::endl;
        return false;
    }
    return true;
}

void Character::DefineAnimations()
{
    walkingAnimationDown.setSpriteSheet(texture);
    walkingAnimationDown.addFrame(sf::IntRect(32, 0, 32, 32));
    walkingAnimationDown.addFrame(sf::IntRect(64, 0, 32, 32));
    walkingAnimationDown.addFrame(sf::IntRect(32, 0, 32, 32));
    walkingAnimationDown.addFrame(sf::IntRect( 0, 0, 32, 32));

    walkingAnimationLeft.setSpriteSheet(texture);
    walkingAnimationLeft.addFrame(sf::IntRect(32, 32, 32, 32));
    walkingAnimationLeft.addFrame(sf::IntRect(64, 32, 32, 32));
    walkingAnimationLeft.addFrame(sf::IntRect(32, 32, 32, 32));
    walkingAnimationLeft.addFrame(sf::IntRect( 0, 32, 32, 32));

    walkingAnimationRight.setSpriteSheet(texture);
    walkingAnimationRight.addFrame(sf::IntRect(32, 64, 32, 32));
    walkingAnimationRight.addFrame(sf::IntRect(64, 64, 32, 32));
    walkingAnimationRight.addFrame(sf::IntRect(32, 64, 32, 32));
    walkingAnimationRight.addFrame(sf::IntRect( 0, 64, 32, 32));

    walkingAnimationUp.setSpriteSheet(texture);
    walkingAnimationUp.addFrame(sf::IntRect(32, 96, 32, 32));
    walkingAnimationUp.addFrame(sf::IntRect(64, 96, 32, 32));
    walkingAnimationUp.addFrame(sf::IntRect(32, 96, 32, 32));
    walkingAnimationUp.addFrame(sf::IntRect( 0, 96, 32, 32));
    
    currentAnimation = &walkingAnimationDown;
}

void Character::Move(tmx::MapLoader &map)
{
    sf::Vector2f movement(0.f, 0.f);
    frameTime = frameClock.restart();
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            auto future = animatedSprite.getGlobalBounds();
            future.top = future.top - 1;
            if(!Character::DetectCollision(map, future))
            {
                currentAnimation = &walkingAnimationUp;
                movement.y -= speed;
                noKeyWasPressed = false;
            }
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            auto future = animatedSprite.getGlobalBounds();
            future.top = future.top + 1;
            if(!Character::DetectCollision(map, future))
            {
                currentAnimation = &walkingAnimationDown;
                movement.y += speed;
                noKeyWasPressed = false;
            }
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            auto future = animatedSprite.getGlobalBounds();
            future.left = future.left - 1;
            if(!Character::DetectCollision(map, future))
            {
                currentAnimation = &walkingAnimationLeft;
                movement.x -= speed;
                noKeyWasPressed = false;
            }
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            auto future = animatedSprite.getGlobalBounds();
            future.left = future.left + 1;
            if(!Character::DetectCollision(map, future))
            {
                currentAnimation = &walkingAnimationRight;
                movement.x += speed;
                noKeyWasPressed = false;
            }
        }
        animatedSprite.play(*currentAnimation);
        animatedSprite.move(movement *  frameTime.asSeconds());

        // if no key was pressed stop the animation
        if (noKeyWasPressed)
        {
            animatedSprite.stop();
        }
        noKeyWasPressed = true;
        animatedSprite.update(frameTime);
}

bool Character::DetectCollision(tmx::MapLoader &map, sf::Rect<float> position) {
    bool collision = false;
    const auto& layers = map.getLayers();
    for(const auto& layer : layers)
    {
        if(layer.name == "collision")
        {
            for(const auto& object : layer.objects)
            {
                sf::Rect<float> objRect = object.getAABB();
                if(position.intersects(objRect)) return true;
            }
        }
        if(layer.name == "trigger")
        {
            for(const auto& object : layer.objects)
            {
                sf::Rect<float> objRect = object.getAABB();
                if(position.intersects(objRect))
                {
                    std::cout << "An Event was triggered!" << std::endl;
                    
                    map.load("shader_example.tmx");
                    
                }
            }
        }
    }
    return (collision);
}

Character::~Character()
{
}

