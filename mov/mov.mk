##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=mov
ConfigurationName      :=Debug
WorkspacePath          :=/home/kevin/Documents/movgame
ProjectPath            :=/home/kevin/Documents/movgame/mov
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Kevin Cantrell
Date                   :=09/12/17
CodeLitePath           :=/home/kevin/.codelite
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)SFML_STATIC 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="mov.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)/usr/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)sfml-graphics $(LibrarySwitch)sfml-window $(LibrarySwitch)sfml-audio $(LibrarySwitch)sfml-network $(LibrarySwitch)sfml-system $(LibrarySwitch)GL $(LibrarySwitch)tmxlite $(LibrarySwitch)tmx-loader 
ArLibs                 :=  "sfml-graphics" "sfml-window" "sfml-audio" "sfml-network" "sfml-system" "GL" "tmxlite" "tmx-loader" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)/usr/lib 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -O0 -std=c++14 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/Implementation.cpp$(ObjectSuffix) $(IntermediateDirectory)/App.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Character_Character.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Character_PolarVector.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Character_Animation.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) main.cpp

$(IntermediateDirectory)/Implementation.cpp$(ObjectSuffix): Implementation.cpp $(IntermediateDirectory)/Implementation.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/Implementation.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Implementation.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Implementation.cpp$(DependSuffix): Implementation.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Implementation.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Implementation.cpp$(DependSuffix) -MM Implementation.cpp

$(IntermediateDirectory)/Implementation.cpp$(PreprocessSuffix): Implementation.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Implementation.cpp$(PreprocessSuffix) Implementation.cpp

$(IntermediateDirectory)/App.cpp$(ObjectSuffix): App.cpp $(IntermediateDirectory)/App.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/App.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/App.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/App.cpp$(DependSuffix): App.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/App.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/App.cpp$(DependSuffix) -MM App.cpp

$(IntermediateDirectory)/App.cpp$(PreprocessSuffix): App.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/App.cpp$(PreprocessSuffix) App.cpp

$(IntermediateDirectory)/src_Character_Character.cpp$(ObjectSuffix): src/Character/Character.cpp $(IntermediateDirectory)/src_Character_Character.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/src/Character/Character.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Character_Character.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Character_Character.cpp$(DependSuffix): src/Character/Character.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Character_Character.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Character_Character.cpp$(DependSuffix) -MM src/Character/Character.cpp

$(IntermediateDirectory)/src_Character_Character.cpp$(PreprocessSuffix): src/Character/Character.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Character_Character.cpp$(PreprocessSuffix) src/Character/Character.cpp

$(IntermediateDirectory)/src_Character_PolarVector.cpp$(ObjectSuffix): src/Character/PolarVector.cpp $(IntermediateDirectory)/src_Character_PolarVector.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/src/Character/PolarVector.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Character_PolarVector.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Character_PolarVector.cpp$(DependSuffix): src/Character/PolarVector.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Character_PolarVector.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Character_PolarVector.cpp$(DependSuffix) -MM src/Character/PolarVector.cpp

$(IntermediateDirectory)/src_Character_PolarVector.cpp$(PreprocessSuffix): src/Character/PolarVector.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Character_PolarVector.cpp$(PreprocessSuffix) src/Character/PolarVector.cpp

$(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(ObjectSuffix): src/Character/AltSpriteHolder.cpp $(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/src/Character/AltSpriteHolder.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(DependSuffix): src/Character/AltSpriteHolder.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(DependSuffix) -MM src/Character/AltSpriteHolder.cpp

$(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(PreprocessSuffix): src/Character/AltSpriteHolder.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Character_AltSpriteHolder.cpp$(PreprocessSuffix) src/Character/AltSpriteHolder.cpp

$(IntermediateDirectory)/src_Character_Animation.cpp$(ObjectSuffix): src/Character/Animation.cpp $(IntermediateDirectory)/src_Character_Animation.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/src/Character/Animation.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Character_Animation.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Character_Animation.cpp$(DependSuffix): src/Character/Animation.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Character_Animation.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Character_Animation.cpp$(DependSuffix) -MM src/Character/Animation.cpp

$(IntermediateDirectory)/src_Character_Animation.cpp$(PreprocessSuffix): src/Character/Animation.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Character_Animation.cpp$(PreprocessSuffix) src/Character/Animation.cpp

$(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(ObjectSuffix): src/Character/AnimatedSprite.cpp $(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/kevin/Documents/movgame/mov/src/Character/AnimatedSprite.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(DependSuffix): src/Character/AnimatedSprite.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(DependSuffix) -MM src/Character/AnimatedSprite.cpp

$(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(PreprocessSuffix): src/Character/AnimatedSprite.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_Character_AnimatedSprite.cpp$(PreprocessSuffix) src/Character/AnimatedSprite.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


