#include "App.h"
#include "src/Character/AltSpriteHolder.hpp"
#include "src/Character/PolarVector.hpp"
#include "src/Character/Character.h"

App::App(char const* title, unsigned int width, unsigned int height) {
	_window = new sf::RenderWindow(sf::VideoMode(width, height), title);
	_event = new sf::Event;
	_clock = new sf::Clock;
	_frame = new sf::Time;
}

App::App(const App &o) {
	_window = o._window;
	_event = o._event;
	_clock = o._clock;
	_frame = o._frame;
}

App& App::operator=(App const& o) {
	_window = o._window;
	_event = o._event;
	_clock = o._clock;
	_frame = o._frame;
	return *this;
}

App::~App() {

}

void App::run() {
	if (init()) {
        Character* hero = new Character();
        hero->SetTexture();
        hero->DefineAnimations();
        
    //create map loader and load map
	tmx::MapLoader ml("maps\\");
	ml.load("zelda.tmx");
	sf::Clock frameClock, shaderClock;

	//load shader and set layer to use it
	sf::Shader waterEffect;
    if (!waterEffect.loadFromFile("water.frag", sf::Shader::Fragment))
    {
        std::cout << "error occured loading shader" << std::endl;
    }

	ml.setLayerShader(2u, waterEffect);
    
    
		while (_window->isOpen()) {
			while (_window->pollEvent(*_event)) {
				handleEvent(*_event);
			}
			*_frame = _clock->restart();

			//update(*_frame);
			//render(*_frame);
            		//update shader
		waterEffect.setParameter("time", shaderClock.getElapsedTime().asSeconds());
        
        hero->Move(ml);
            
        
		//draw
		frameClock.restart();
		_window->clear();
		_window->draw(ml);
        //_window->draw(sprite);
        _window->draw(hero->animatedSprite);
		_window->display();
		}
	}
}

bool App::init() {
	_window->setVerticalSyncEnabled(true);
	return true;
}

void App::update(sf::Time const &time) {

}

void App::render(sf::Time const &time) {
	//glClear(GL_COLOR_BUFFER_BIT);
}

void App::exit() {
	_window->close();
}

void App::handleEvent(sf::Event const& event) {
	switch (event.type) {
		case sf::Event::Closed:
			exit();
			break;
		case sf::Event::KeyPressed:
			if (event.key.code == sf::Keyboard::Q) {
				exit();
			}
            
            if(event.key.code == sf::Keyboard::Up) {

            } else if(event.key.code == sf::Keyboard::Down) {
                
            } else if(event.key.code == sf::Keyboard::Left) {
                
            } else if(event.key.code == sf::Keyboard::Right) {
                
            }
            
			break;
		default:
			break;
	}
}