.PHONY: clean All

All:
	@echo "----------Building project:[ mov - Debug ]----------"
	@cd "mov" && "$(MAKE)" -f  "mov.mk"
clean:
	@echo "----------Cleaning project:[ mov - Debug ]----------"
	@cd "mov" && "$(MAKE)" -f  "mov.mk" clean
